
import numpy as np
import pandas as pd
import math
import random
import string
import re

from string import punctuation

from scipy import sparse

from sklearn.preprocessing import StandardScaler, LabelEncoder, OneHotEncoder
from sklearn.model_selection import train_test_split, cross_val_score, cross_val_predict
from sklearn.metrics import accuracy_score
from sklearn.ensemble import RandomForestClassifier
from sklearn.feature_extraction.text import CountVectorizer, TfidfVectorizer
from sklearn.model_selection import RandomizedSearchCV, GridSearchCV
from sklearn.linear_model import LogisticRegression
from sklearn.svm import SVC

import seaborn as sns

import matplotlib.pyplot as plt

import xgboost as xgb

from wordcloud import WordCloud

from nltk.corpus import stopwords
from nltk import word_tokenize
from nltk import regexp_tokenize
from nltk.stem import SnowballStemmer
stemmer = SnowballStemmer('english')

def filter_sentiment(df, sentiment):
    """
    Input: 
        - df that has a column called 'airline sentiment'
        - sentiment: (string) one of 'positive', 'neutral', 'negative'
    Output: 
        - df filtered by string defined by input 'sentiment'
    """
    X = df.copy()
    X = X[X['airline_sentiment'].str.contains(sentiment)]
    return(X)

def convert_wordlists_to_string(text_series, return_split = False):
    """
    Input: 
        - text_series (series of list) of strings with tokenized tweets
        - return_split (bool) should output be 'tokenized'?
    Output: 
        - long string of words
    """
    if return_split:
        words = " ".join([' '.join(x) for x in text_series]).split()
    else:
        words = " ".join([' '.join(x) for x in text_series])
    return(words)

def clean_and_tokenize_text(raw_text, stem_words, remove_stops):
    '''
    Inputs:
        - raw_text (string)
        - stem_words (bool): stem words or not
        - remove_stops (bool): remove stopwords or not
    Output:
        - tokenized, cleaned text
    '''
    ## not to remove when removing stopwords 
    whitelist = ["not", "no"]
    ## set stopwords
    stop_words = set(stopwords.words('english'))
    
    ### clean text
    ## remove airport abbreviations
    text  = re.sub("[A-Z]{2,3}", "", raw_text)
    ## remove airline mention, convert to lower
    text  = re.sub("@\w+\ ", "", text.lower())
    ## remove 2 or 3 dots
    text = re.sub(r"\.\.|\.\.\.", "", text)
    ## remove anything that starts with //
    text = re.sub(r"//.*", "", text)
    ## replace abbreviations
    text = re.sub(r"what's", "what is ", text)
    text = re.sub(r"\'s", " is", text)
    text = re.sub(r"\'ve", " have ", text)
    text = re.sub(r"can't", "cannot ", text)
    text = re.sub(r"n't", " not ", text)
    text = re.sub(r"i'm", "i am ", text)
    text = re.sub(r"\'re", " are ", text)
    text = re.sub(r"\'d", " would ", text)
    text = re.sub(r"\'ll", " will ", text)

    ### tokenize
    tokens = word_tokenize(text)
    ## remove punctuation from each token
    tokens = [word for word in tokens if not word in string.punctuation]
    ## remove remaining tokens that are not alphabetic
    tokens = [word for word in tokens if word.isalpha()]
    ## filter out stop words, but not whitelisted
    if remove_stops:
        tokens = [word for word in tokens if (word not in stop_words or word in whitelist)]
    ## filter out short tokens
    tokens = [word for word in tokens if len(word) > 1]
    ## stemming
    if stem_words:
        tokens = [stemmer.stem(word) for word in tokens] 

    return tokens

def count_regex(pattern, tweet):
    ''' 
    Counts the occurence of pattern in tweet
    '''
    return len(re.findall(pattern, tweet))

def generate_features(X):
    ''' 
    Inputs:
        - X: (df) input data
    Output:
        - df with count statistis of several features
    '''
    ## count statists
    text_length = X['text'].apply(lambda x: count_regex(r'\w+', x)) 
    #num_mentions = X['text'].apply(lambda x: count_regex(r'@\w+', x))
    num_hashtags = X['text'].apply(lambda x: count_regex(r'#\w+', x))
    #num_capital_words = X['text'].apply(lambda x: count_regex(r'\b[A-Z]{2,}\b', x))
    num_excl_quest_marks = X['text'].apply(lambda x: count_regex(r'!|\?', x))
    num_urls = X['text'].apply(lambda x: count_regex(r'http.?://[^\s]+[\s]?', x))
    num_times = X['text'].apply(lambda x: count_regex(r'hr | hour | hours | min | mins | minutes', x.lower()))

    ## detect and append smileys 
    smileys = [':)', ':D', ':-D', ';p', ';-)']
    has_smiley = X['text'].apply(lambda x: 1 if any(smiley in x for smiley in smileys) else 0)
    
    ## one-hot encode airline
    one_hot_airline = pd.get_dummies(X['airline'])

    count_features = pd.DataFrame({
        'text_length': text_length,
        #'num_mentions': num_mentions,
        'num_hashtags': num_hashtags,
        #'num_capital_words': num_capital_words,
        'num_excl_quest_marks': num_excl_quest_marks,
        'num_urls': num_urls,
        'num_times': num_times,
        'has_smiley': has_smiley
    })

    return(pd.concat([count_features, one_hot_airline], axis = 1))


def show_dist(df, col):
    """
    Plots count statistics per sentiment for given column
    Inputs:
        - df: raw train data
        - col: (string) column name
    """
    print('Descriptive stats for {}'.format(col))
    print('-'*(len(col)+22))
    print(df.groupby('airline_sentiment')[col].describe())
    bins = np.arange(df[col].min(), df[col].max() + 1)
    g = sns.FacetGrid(df, col='airline_sentiment', size=5, hue='airline_sentiment', palette="PuBuGn_d")
    g = g.map(sns.distplot, col, kde=False, norm_hist=True, bins=bins)
    plt.show()

def fill_emo_matrix(emo_matrix,emolist,tweet,tweet_index):
        """
        Takes a matrix of zeros and fills with 1 the column of the correspondig emojis on the row of the tweet
        Inputs: 
            - emolist: list of emojis
            - tweet: the full text (with the emojis)
            - tweet_index: to know in which row of the emo_matrix we should store the value

        Output:
            - non-zero emo_matrix 
        """
        for emo in emolist:
            if emo in tweet:
                emo_matrix[tweet_index,emolist.index(emo)]=1
        return emo_matrix


def get_emo_matrix(df, emolist):
    ''' 
    Inputs:
        - df: data matrix
        - emolist: list of emojis
    Outputs:
        - emo_matrix: matrix with first dimension equal df.shape[0] and one-hot encoding for emojis
    '''
    X = df.copy()

    emo_matrix=np.zeros((X.shape[0],np.shape(emolist)[0]))

    for index,tweet in zip(X.index , X['text']):
        emo_matrix=fill_emo_matrix(emo_matrix,emolist,tweet,index)
    
    return(emo_matrix)

def create_ngram_matrix(text_series, vectorizer, train):
    """
    Inputs: 
        - text_series: (series) of tweets
        - vectorizer: initialized tfidf or count vectorizer from sklearn
        - train (bool): training or test set?
    Outputs:
        - X: (ndarray) ngram tfidf/count matrix
    """
    words = [" ".join(word) for word in text_series]
    if train:
        X = vectorizer.fit_transform(words).toarray()
    else:
        X = vectorizer.transform(words).toarray()
    return(X)

def generate_train_test_feature_matrices(data_train, data_test, emolist, n, sparse_, max_feat = None, tfidf = False):
    """
    Generates the feature matrices for both train and test data
    Inputs:
        - data_train (df): raw training data
        - data_test (df): raw test data
        - emolist: list of emojis
        - n (int): n-gram
        - sparse_ (bool): return sparse matrix
        - max_feat (int): maximum features to consider in n-gram matrix
        - tfidf (bool): count or tfidf n-gram matrix
    Outputs:
        - X_train (ndarray): feature matrix, train
        - X_test (ndarray): feature matrix, test
    """
    if tfidf:
        vectorizer = TfidfVectorizer(ngram_range = (1, n),
                                     max_features = max_feat)
    else:
        vectorizer = CountVectorizer(ngram_range = (1, n),
                                     max_features = max_feat)                                     
    X_train = generate_feature_matrix(data_train, emolist, True, vectorizer, sparse_)
    X_test = generate_feature_matrix(data_test, emolist, False, vectorizer, sparse_)
    return(X_train, X_test)

def generate_feature_matrix(data, emolist, train, vectorizer, sparse_):
    """
    Generates either train or test feature matrix
    Inputs:
        - see function above (generate_train_test_feature_matrices)
    Output:
        - X (ndarray): feature matrix
    """
    X = data.copy()
    ## Extract features from text
    X_feat = generate_features(X).values
    ## Clean tweets
    X['text']=X['text'].apply(lambda x: clean_and_tokenize_text(x, stem_words = True, remove_stops = True))    
    ## Generate either tfidf or count matrix (document-term matrix)
    X_ngram = create_ngram_matrix(X['text'], vectorizer, train)
    ## Generate emoji feature matrix
    X_emo = get_emo_matrix(X, emolist)
    ## Drop useless columns from data
    if train:
        X = X.drop(['Id','text', 'tweet_coord', 'tweet_location', 'user_timezone', 'airline', 'airline_sentiment'], axis = 1)
    else:
        X = X.drop(['Id','text', 'tweet_coord', 'tweet_location', 'user_timezone', 'airline'], axis = 1)
    
    ## Concatenate the different feature matrices
    X = np.c_[X, X_feat, X_emo, X_ngram]
    ## Sparse matrix
    if sparse_:
        X = sparse.csc_matrix(X)
    return(X)

def convert_sentiment_to_num(sentiment):
    if sentiment == 'positive':
        return(2)
    if sentiment == 'neutral':
        return(1)
    if sentiment == 'negative':
        return(0)

def convert_num_to_sentiment(num):
    if num == 2:
        return('positive')
    if num == 1:
        return('neutral')
    if num == 0:
        return('negative')

def grid_search_classifier(X, y, parameters, crossval, classifier = 'rf'):
        """
        Inputs:
            - X (ndarray): feature matrix, whole dataset
            - y (list or ndarray): target vector, whole dataset
            - parameters (dict): parameter grid
            - crossval (int): k-fold
            - classifier (str): one of 'rf', 'logistic', or 'svm'
        Outputs:
            - clf_grid: trained and cross validated grid search classifier
        """
        if classifier == 'rf':
            clf = RandomForestClassifier()
        elif classifier == 'logistic':
            clf = LogisticRegression()
        elif classifier == 'svm':
            clf = SVC()
        clf_grid = GridSearchCV(clf, parameters,cv=crossval)
        clf_grid.fit(X,y)
        return(clf_grid)

def prepare_and_save_submission(predictions, filename, submission):
    """
    Inputs:
        - predictions (series): prediction result from classifier
        - filename (string)
        - submission (df): sample_submission dataframe
    """
    submission['Prediction'] = predictions
    submission['Prediction'] = [convert_num_to_sentiment(prediction)\
                                for prediction in submission['Prediction'].copy()]
    submission.to_csv(filename, index = False)
    return(submission)

def construct_embedding_matrix(w2v_model, embedding_dim, word_index):
    """ 
    Construct embedding for every word based on the embeddings corpus
    In matrix every row represents the embedding of a word

    Inputs:
        - w2v_model: word2vec model
        - embedding_dim (int): dimension of the embedding vector for each word
        - word_index (dict): word with its corresponding index in tokenizer
    """
    words = len(word_index)+1
    embedding_matrix = np.zeros((words, embedding_dim))
    for word, i in word_index.items():
        if word in w2v_model.vocab:
            try:
                embedding_matrix[i] = w2v_model.word_vec(word)
            except:
                print(word, i)
    print('Null word embeddings: %d' % np.sum(np.sum(embedding_matrix, axis=1) == 0))
    print("These are words that don't exist in the Google Corpus but in our data!!!")
    return(embedding_matrix)


def plot_wordcloud(data, sentiment):
    """
    Inputs:
        - data (df) training data
        - sentiment (string): one of 'negative', 'neutral', or 'positive' 
    """
    df = filter_sentiment(data, sentiment)
    words = df['text'].apply(lambda x: clean_and_tokenize_text(x, remove_stops=True, stem_words=False))
    words = convert_wordlists_to_string(words)

    wordcloud = WordCloud(
                      background_color='black',
                      width=3000,
                      height=2500
                     ).generate(words)

    plt.figure(1,figsize=(12, 12))
    plt.imshow(wordcloud)
    plt.axis('off')
    plt.show()