# Sentiment Analysis of Twittter Airline Tweets
in-class competition: https://www.kaggle.com/c/airline-sentiment-prediction
equivalent (but with slightly changed dataset) to: https://www.kaggle.com/crowdflower/twitter-airline-sentiment

The notebook describing the analysis is
- notebook_tech_test.ipynb

I also wrote a library from which I am using functions
- library.py
